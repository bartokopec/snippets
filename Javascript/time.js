export const getDateDifferenceVerbally = (datetime) => {
// If you just subtract the dates, it won't work across 
// the Daylight Savings Time Boundary. 
// This drops all the hours to make sure you get a day 
// and eliminates any DST problem by using UTC.

  const dayPeriod = 1000*60*60*24;
  const date = new Date(datetime);
  const now = new Date(Date.now());
  const nowUtc = Date.UTC(now.getFullYear(), now.getMonth(), now.getDate());
  const dateUtc = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());

  const diff = (nowUtc - dateUtc) / dayPeriod;
  if(diff < 1){
    return 'today'
  }
  if(diff < 2){
    return 'yesterday'
  }
  // etc
  if(diff < 7){
    return 'week'
  }
  return datetime
}