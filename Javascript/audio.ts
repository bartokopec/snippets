export const useAudio = (elementId: string) => {
  const play = () => {
    const element = document.querySelector(`#${elementId}`) as HTMLMediaElement;
    if (!element) {
      throw new Error();
    }

    const audioContext = new AudioContext();
    const source = audioContext.createMediaElementSource(element);
    source.connect(audioContext.destination);
    element.volume = 0.5;
    element.play();

    setTimeout(() => {
      source.disconnect();
    }, 400); // real sound time - some audios have silent seconds at the end
  };

  return {
    play
  };
};

// EXAMPLE
/*
HTML:
...
<audio id="audio-error-tone" src="path"></audio>
...

Script:
...
const { play } = useAudio('audio-error-tone');
play();
...
*/